# Readme
This template is meant to be a starting point for next-gen js/html applications.

## Instructions
* Install [nodejs](https://nodejs.org/) (if not already installed)
* Download or clone this repository
* Run `npm install` in this directory to install all required packages
* Run `npm run bundle-dev` to launch the dev webserver
* Go to [http://localhost:8080](http://localhost:8080)
* Modify the source code with your favorite editor and see how the website reloads automatically
	* I can highly recommend [VS Code](https://code.visualstudio.com/) as editor as it features proper intellisense for TypeScript

## Technologies
The following technologies are bundled with this template and ready to use:
* [React](https://facebook.github.io/react/), a UI framework, enhanced HTML
* [Mobx](https://mobx.js.org/), features observable models
* [Webpack](https://webpack.github.io/) (Only for development), makes developing und bundling much easier
* [TypeScript](http://www.typescriptlang.org/) (Only for development), enhanced JavaScript with a proper type system
* [Sass](http://sass-lang.com/) (Only for development), enhanced CSS